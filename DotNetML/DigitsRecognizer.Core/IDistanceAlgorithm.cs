﻿using System.Collections.Generic;

namespace Techniqly.DigitsRecognizer.Core
{
    public interface IDistanceAlgorithm
    {
        double Between(int[] first, int[] second);
    }
}