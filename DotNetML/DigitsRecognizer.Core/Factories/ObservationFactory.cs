﻿using System;
using System.Linq;
using Techniqly.DigitsRecognizer.Core.Models;

namespace Techniqly.DigitsRecognizer.Core.Factories
{
    public sealed class ObservationFactory
    {
        public static Observation Create(string data)
        {
            var items = data.Split(',');
            var label = items[0];
            var pixels = items.Skip(1).Select(x => Convert.ToInt32(x)).ToArray();

            return new Observation(label, pixels);
        }
    }
}