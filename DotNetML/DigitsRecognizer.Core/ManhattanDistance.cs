﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Techniqly.DigitsRecognizer.Core
{
    public sealed class ManhattanDistance : IDistanceAlgorithm
    {
        public double Between(int[] first, int[] second)
        {
            if (first.Length != second.Length)
            {
                throw new ArgumentException("Input arrays must have the same dimension.");
            }

            var distance = 0;

            for (var i = 0; i < first.Length; i++)
            {
                var diff = first[i] - second[i];

                if (diff < 0)
                {
                    diff *= -1;
                }

                distance += diff;
            }

            return distance;
        }
    }
}
