﻿using System.Collections.Generic;
using Techniqly.DigitsRecognizer.Core.Models;

namespace Techniqly.DigitsRecognizer.Core
{
    public interface IClassifier
    {
        void Train(Observation[] trainingSet);
        string Predict(int[] pixels);
    }
}