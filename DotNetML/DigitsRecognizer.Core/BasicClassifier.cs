﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Techniqly.DigitsRecognizer.Core.Models;

namespace Techniqly.DigitsRecognizer.Core
{
    public sealed class BasicClassifier : IClassifier
    {
        private readonly IDistanceAlgorithm _distanceAlgorithm;
        private Observation[] _trainingSet;
        private static readonly object _lock = new object();

        public BasicClassifier(IDistanceAlgorithm distanceAlgorithm)
        {
            _distanceAlgorithm = distanceAlgorithm;
        }

        public void Train(Observation[] trainingSet)
        {
            _trainingSet = trainingSet;
        }

        public string Predict(int[] pixels)
        {
            var indexOfMin = 0L;
            var minDistance = double.MaxValue;

            Parallel.ForEach(
                _trainingSet,
                (observation, state, index) =>
                {
                    var distance = _distanceAlgorithm.Between(observation.Pixels, pixels);

                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        indexOfMin = index;
                    }

                });
            
            return _trainingSet[indexOfMin].Label;
        }
    }
}