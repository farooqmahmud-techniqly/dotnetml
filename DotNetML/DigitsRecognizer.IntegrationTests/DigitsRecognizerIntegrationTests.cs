﻿using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.DigitsRecognizer.Core;
using Techniqly.DigitsRecognizer.Core.Factories;
using Techniqly.DigitsRecognizer.Core.Models;
using Techniqly.DigitsRecognizer.IntegrationTests.Properties;

namespace Techniqly.DigitsRecognizer.IntegrationTests
{
    [TestFixture]
    public class DigitsRecognizerIntegrationTests
    {
        [Test]
        public void VerifyTrainingSampleFile()
        {
            var observations = ReadObservationsFromTrainingSetFile();
            observations.Count().Should().Be(5000);
        }

        [Test]
        public void VerifyValidationSampleFile()
        {
            var observations = ReadObservationsFromValidationSetFile();
            observations.Count().Should().Be(500);
        }

        [Test]
        public void EvaluateBasicClassifierWithManhattanDistanceAlgorithm()
        {
            var expectedTrainingSetObservations = 5000;

            var trainingSet = ReadObservationsFromTrainingSetFile();
            trainingSet.Count().Should().Be(expectedTrainingSetObservations);

            var expectedValidationSetObservations = 500;

            var validationSet = ReadObservationsFromValidationSetFile();
            validationSet.Count().Should().Be(expectedValidationSetObservations);

            var classifier = new BasicClassifier(new ManhattanDistance());
            classifier.Train(trainingSet.ToArray());

            var score = 0.0;

            foreach (var observation in validationSet)
            {
                var prediction = classifier.Predict(observation.Pixels);

                if (prediction == observation.Label)
                {
                    score += 1.0;
                }
            }

            var average = score / expectedValidationSetObservations;

            average.Should().Be(0.934);
        }

        private static IEnumerable<Observation> ReadObservationsFromTrainingSetFile()
        {
            return ReadObservationsFromResource("Techniqly.DigitsRecognizer.IntegrationTests.Resources.trainingsample.csv");
        }

        private static IEnumerable<Observation> ReadObservationsFromValidationSetFile()
        {
            return ReadObservationsFromResource("Techniqly.DigitsRecognizer.IntegrationTests.Resources.validationsample.csv");
        }

        private static IEnumerable<Observation> ReadObservationsFromResource(string resourceName)
        {
            var fs = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            var sr = new StreamReader(fs);
            var observations = new List<Observation>();

            string line = null;

            //skip first line
            line = sr.ReadLine();

            while (line != null)
            {
                line = sr.ReadLine();

                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                observations.Add(ObservationFactory.Create(line));
            }

            return observations;
        }
    }
}
